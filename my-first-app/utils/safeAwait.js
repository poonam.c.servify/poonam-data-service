function safeAwait(promise) {
    const wait = promise
    .then(function (data) {
        return {
                data: data,
        };
    })
    .catch(function (error) {
        return {
        error: error,
        };
    });
    return wait;
}

module.exports = safeAwait;