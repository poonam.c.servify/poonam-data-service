const knex = require('knex');
const env = require('./env');


const connection = knex({
    client: 'mysql2',
    connection: {
      host : env.DB_HOST,
      user :env.DB_USER,
      password : env.DB_PASSWORD,
      database : env.DB_DATABASE,
      port:env.DB_PORT
    }
  });
  module.exports = connection;