const mysql  = require('./mysql');
const knex = require('./knex.js');

exports.connection = mysql;
exports.knex = knex;

