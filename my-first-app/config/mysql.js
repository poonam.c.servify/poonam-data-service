const mysql2 = require('mysql2');
const env = require('./env');

const connection = mysql2.createConnection({
    user: env.DB_USER,
    password: env.DB_PASSWORD,
    port: env.DB_PORT,
    host: env.DB_HOST,
    database: env.DB_DATABASE
});
module.exports = connection;