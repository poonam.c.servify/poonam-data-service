const axios = require('axios');

module.exports = function (req, res, next) {
    if (!req.headers['x-token']) {
        return res.status(401).json({
            message: "Please provide the token"
        });
    }
    const token = req.headers['x-token'];
    const data = JSON.stringify({
        "token": token
    });

    const config = {
        method: 'post',
        url: process.env.AUTH_URL,
        headers: {
            'x-token': token,
        },
        data: data
    };

    axios(config)
        .then(function ({data}) {
            console.log(data);
            req.user = data.data;
            next();
        })
        .catch(function ({response,message}) {
            const {status,data,statusText} = response || {};
            console.log(status,data,statusText,message);
            if (status) {
                return res.status(status).json({
                    message: data.message
                });
            }
            if(!status){
                res.status(500).json({
                    message: "Internal Error"
                });
            }
        });
}