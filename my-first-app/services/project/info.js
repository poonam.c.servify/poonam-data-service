const {connection } = require('../../config');
module.exports = (body)=>{
    const getUser = `
        SELECT * FROM user WHERE id = ?
    `
    const query =
    `
    SELECT info.extra_info, info.extra_info_value 
    FROM 
    user as u
    INNER JOIN info on u.id = info.user_id
    WHERE info.user_id = ?;
    `;
    return new Promise(function(resolve,reject){
        connection.query(getUser, [body.user_id], (err, user) => {
            if(err){
                return reject(err);
            }
            connection.query(query,[body.user_id],(err, extra)=>{
                if(err){
                    return reject(err);
                }
                const userInfoMapping = {}
                extra.forEach(item =>{
                    if(userInfoMapping[item.extra_info]){
                        const value = userInfoMapping[item.extra_info];
                        if(Array.isArray(value)){
                            userInfoMapping[item.extra_info].push(item.extra_info_value)
                        }else{
                            userInfoMapping[item.extra_info] = [value,item.extra_info_value]; 
                        }
                    }else{
                        userInfoMapping[item.extra_info] = item.extra_info_value;
                    }
                });
                resolve({user, extra:userInfoMapping});    
            });
        });    
    });
}


