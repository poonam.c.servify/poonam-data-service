const { connection } = require('../../config');

module.exports = 
    function addInInfo( arr1 = []){
        const query1 = `INSERT INTO info (user_id,extra_info,extra_info_value) VALUES`;
        let queryParams = [];
        arr1.forEach(item =>{
            queryParams.push(...item);
        });
        let queryPlaceholder =[];
        arr1.forEach(item => {
            queryPlaceholder.push("(?,?,?)");
        });

        const finalQuery = `${query1} ${queryPlaceholder.join(',')};`;
        return new Promise(function(resolve, reject){
            connection.query(finalQuery, [...queryParams],(err, data) => {
                if(err) {
                    console.log(err);
                    return reject(err);
                }
                resolve(data);
            });
        });
    }


