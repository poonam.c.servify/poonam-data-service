const { connection } = require('../../config');

module.exports = (body) =>{
    const query = `
        INSERT INTO user (name, email, gender, age) VALUES(?,?,?,?);`;
    return new Promise(function(resolve,reject){
        connection.query(query,[body.name,body.email,body.gender,body.age],(err,result)=>{
            if(err){
                return reject(err);
            }
            resolve(result);
        });
    });
}

