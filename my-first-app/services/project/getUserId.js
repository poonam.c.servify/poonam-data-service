const { connection } = require('../../config');

module.exports = (userId) =>{
    const query = `
    SELECT 
    u.id as user_id
    FROM user u WHERE u.id =?;
    `;

    return new Promise(function(resolve,reject){
        connection.query(query,userId,(err,result)=>{
            if(err){
                return reject(err);
            }
            resolve(result);
        });
    });
}