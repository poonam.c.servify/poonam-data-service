module.exports ={
    insertUser: require('./insertUser'),
    getUsers: require('./getUsers'),
    getUserInfo: require('./getUserInfo'),
    getUserid: require('./getUserId'),
    userInfo: require('./info'),
    addUser: require('./addUser'),
    user_info: require('./user-info')
}


