const addUser = require('./addUser');
const {safeAwait} = require('../../utils');

module.exports = async (body) => {
        const keys = ["name", "email", "gender", "age"]; // fixed keys 
        const bodyKeys = Object.keys(body);
        const extrasKeys = bodyKeys.filter(key => !keys.includes(key)); //filter out the extra keys
        const extrakeyValue = {};
        extrasKeys.forEach(item => {
            extrakeyValue[item] = body[item];
        });
        const kepMapentry = Object.entries(extrakeyValue);
        kepMapentry.forEach(item => {

            let splitkey = item[1].toString().split(',');
            item[1] = splitkey;
        });
        const arr1 = [];
        kepMapentry.forEach(item1 => {
            mapKey = item1[1].map(item => {
                return [item1[0], item];
            });
            arr1.push(...mapKey);
        });
        const fixData = await safeAwait(addUser(body));
        let id = fixData.data.insertId;
        let array2 = [id]; 
    
        const concatUserIdData = [];
        arr1.forEach(item =>{
        let concatArr = array2.concat(item);
        concatUserIdData.push(concatArr);
        });
        return concatUserIdData;

}