const express = require('express');
const {safeAwait} = require('../../utils');
const {getUserid,userInfo,insertUser,user_info} = require('../../services/project');
const router = express.Router();
router.get('/info/:id', async function (req, res, next) {
    if (!+req.params.id) {
        return res.status(422).json({
            message: 'id should be a numeric value',
        });
    }
    const result = await safeAwait(getUserid(req.params.id));
    if (result.error) {
        return res.status(500).json({
            message: "ERROR getting user Info!"
        });
    }
    if (!result.data.length) {
        return res.json({
            message: `No such user found with user id ${req.params.id}`,
            data: {}
        });
    }
    const userID = result.data[0];
    const userInfoData = await safeAwait(userInfo(userID));
    if (userInfoData.error) {
        return res.status(500).json({
            message: 'Error......'
        });
    }
    const {data: {user, extra}} = userInfoData;
    res.json({
        res :{
            ...user[0],
            ...extra
        },
    });
});
router.post('/info', async function (req, res, next) {
    const body = req.body;
    const infoUser =  await safeAwait(user_info(body));
    if(infoUser.error){
        return res.status(500).json({
            msg:"Error adding users Information..!"
        });
    }
    const adduserInfo = await safeAwait(insertUser(infoUser.data));
    if(adduserInfo.error){
        return res.status(500).json({
            msg:"Error inserting user Information!"
        });
    }
    res.json({
        msg:"addding user Information",
        data:adduserInfo.data
    });
});
module.exports = router;

